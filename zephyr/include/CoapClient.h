/*
 * Copyright (c) 2022 Huawei Inc.
 *
 * SPDX-License-Identifier: Apache-2.0
 */

#ifndef __COAPCLIENT_H__
#define __COAPCLIENT_H__

#include <zephyr/net/socket.h>
#include <functional>
#include <string>

#include "eddie.h"

class CoapClient {
private:

    static int sock;

    message_t process_simple_coap_reply(int timeout);

    int send_simple_coap_request(request_t request);

public:

    /**
     * @brief Construct a new Coap Client object using a preinitialized socket
     * 
     * @param sock A preinitialized socket
     */
    CoapClient(int sock);

    /**
     * @brief Construct a new Coap Client object by initializing and binding a new socket
     */
    CoapClient();

    /**
     * @brief Close socket used by the client
     */
    void close_coap_client();

    /**
     * Send a coap request and immediately wait for a response
     * @param request The request to send. This parameter contains information such as destination and data of the request.
     * @param timeout Maximum amount of time in seconds to wait for a response message that matches the given token.
     * @return The response message that matches the request token. If no response is received before timeout, the message
     * will contain a GATEWAY_TIMEOUT status code.
     */
    message_t send_message_and_wait_response(request_t request, int timeout=5);
    
};

#endif