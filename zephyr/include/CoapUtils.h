/*
 * Copyright (c) 2022 Huawei Inc.
 *
 * SPDX-License-Identifier: Apache-2.0
 */

#ifndef __COAPUTILS_H__
#define __COAPUTILS_H__

#include "eddie.h"
#include <zephyr/net/coap.h>

/**
 * Convert a eddie's method_t into the corresponding coap_method in the coap library
 * @param method The eddie's method to convert 
 * @return The corresponding coap_method as defined in the coap library
 */
uint8_t method_to_coap_method(method_t method);

/**
 * Convert a coap_method into eddie's method_t
 * @param method The coap_method to convert
 * @return The corresponding eddie's method_t
 */
method_t coap_method_to_method(uint8_t method);

#endif