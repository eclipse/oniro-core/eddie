/*
 * Copyright (c) 2022 Huawei Inc.
 * Copyright (c) 2018 Intel Corporation
 *
 * SPDX-License-Identifier: Apache-2.0
 */

#include <logging/log.h>
LOG_MODULE_REGISTER(coap_client, LOG_LEVEL_DBG);

#include "CoapClient.h"
#include "CoapUtils.h"

#include <net_private.h>
#include <zephyr/net/coap.h>
#include <string.h>

#define MAX_COAP_MSG_LEN 512

/* CoAP socket fd */
int CoapClient::sock;

////////////////// PRIVATE METHODS ///////////////////////

message_t CoapClient::process_simple_coap_reply(int timeout)
{
	struct coap_packet reply;
	int ret;
	struct sockaddr_storage src_addr;
	socklen_t addrlen = sizeof(src_addr);
	message_t response_message;

	uint8_t * data = (uint8_t *)k_malloc(MAX_COAP_MSG_LEN);
	if (!data) {
		LOG_ERR("Error ENOMEM");
		response_message.status_code = BAD_REQUEST;
		return response_message;
	}

	struct pollfd fd;
	fd.fd = sock;
	fd.events = POLLIN;
	ret = poll(&fd, 1, timeout * 1000); 
	switch (ret) {
		case -1:
			response_message.status_code = BAD_REQUEST;
			return response_message;
		case 0:
			response_message.status_code = GATEWAY_TIMEOUT;
			return response_message;
		default:
			break;
	}

	int rcvd = recvfrom(sock, data, MAX_COAP_MSG_LEN, MSG_DONTWAIT, (struct sockaddr*)&src_addr, &addrlen);

	if (rcvd <= 0) {
		LOG_ERR("recv error=%d errno=%d", rcvd, errno);
		k_free(data);
		response_message.status_code = BAD_REQUEST;
		return response_message;
	}

	// retrieve source ip and port
	char source_ip[64];
	uint16_t port;
	if (src_addr.ss_family == AF_INET6) {
		struct sockaddr_in6* addr = (struct sockaddr_in6*)&src_addr; 
		inet_ntop(AF_INET6, &(addr->sin6_addr), source_ip, 64);
		port = htons(addr->sin6_port);
	}
	else {
		struct sockaddr_in* addr = (struct sockaddr_in*)&src_addr; 
		inet_ntop(AF_INET, &(addr->sin_addr), source_ip, 64);
		port = htons(addr->sin_port);
	}
	LOG_DBG("source ip=%s port=%d", source_ip, port);

	ret = coap_packet_parse(&reply, data, rcvd, NULL, 0);
	if (ret < 0) {
		LOG_ERR("Invalid data received");
		k_free(data);
		response_message.status_code = BAD_REQUEST;
		return response_message;
	}

	uint16_t payload_length;
	const uint8_t * payload = coap_packet_get_payload(&reply, &payload_length);
	uint8_t status_code = coap_header_get_code(&reply);

	response_message.data = std::string((const char*)payload, payload_length);
	response_message.status_code = (response_code_t)status_code;
	response_message.src_host = std::string(source_ip);
	response_message.src_port = std::to_string(port);

	k_free(data);
	return response_message;
}

int CoapClient::send_simple_coap_request(request_t request)
{
	struct coap_packet coap_request_pkt;
	uint8_t *data;
	int r = 0;
	char *token;

	if (request.path == nullptr) request.path = "";
	if (request.query == nullptr) request.query = "";

	char path_copy[strlen(request.path)+1];
	char query_copy[strlen(request.query)+1];
	strcpy(path_copy, request.path);
	strcpy(query_copy, request.query);

	data = (uint8_t *)k_malloc(MAX_COAP_MSG_LEN);
	if (!data) {
		return -ENOMEM;
	}

	uint8_t coap_method = method_to_coap_method(request.method);
	r = coap_packet_init(&coap_request_pkt, data, MAX_COAP_MSG_LEN,
			     COAP_VERSION_1, request.confirmable ? COAP_TYPE_CON : COAP_TYPE_NON_CON,
			     COAP_TOKEN_MAX_LEN, coap_next_token(),
			     coap_method, coap_next_id());
	if (r < 0) {
		LOG_ERR("Failed to init CoAP message");
		goto end;
	}

	token = strtok(path_copy, "/");
	while(token) {
		r = coap_packet_append_option(&coap_request_pkt, COAP_OPTION_URI_PATH,
					      (const uint8_t*)token, strlen(token));
		if (r < 0) {
			LOG_ERR("Unable add path option to request");
			goto end;
		}
		token = strtok(nullptr, "/");
	}

	r = coap_append_option_int(&coap_request_pkt, COAP_OPTION_CONTENT_FORMAT, request.content_format);
	if (r < 0) {
		return -EINVAL;
	}

	token = strtok(query_copy, "&");
	while(token) {
		r = coap_packet_append_option(&coap_request_pkt, COAP_OPTION_URI_QUERY,
					      (const uint8_t*)token, strlen(token));
		if (r < 0) {
			LOG_ERR("Unable add query option to request");
			goto end;
		}
		token = strtok(nullptr, "&");
	}

	if (request.data && request.data_length>0) {
		r = coap_packet_append_payload_marker(&coap_request_pkt);
		if (r < 0) {
			LOG_ERR("Unable to append payload marker");
			goto end;
		}

		r = coap_packet_append_payload(&coap_request_pkt, (uint8_t *)request.data, request.data_length);
		if (r < 0) {
			LOG_ERR("Not able to append payload");
			goto end;
		}
	}

	net_hexdump("Request", coap_request_pkt.data, coap_request_pkt.offset);

#if defined(CONFIG_NET_IPV4)
	struct sockaddr_in destaddr;
	destaddr.sin_family = AF_INET;
	destaddr.sin_port = htons(std::stoul(request.dst_port));
	inet_pton(AF_INET, request.dst_host, &destaddr.sin_addr);
#else
	struct sockaddr_in6 destaddr;
	destaddr.sin6_family = AF_INET6;
	destaddr.sin6_port = htons(std::stoul(request.dst_port));
	destaddr.sin6_scope_id = 0U;
	inet_pton(AF_INET6, request.dst_host, &destaddr.sin6_addr);
#endif
	if(sendto(sock, coap_request_pkt.data, coap_request_pkt.offset, 0, (struct sockaddr*)&destaddr,
       sizeof(destaddr)) < 0) {
		LOG_ERR("sendto error errno=%d", errno);
		goto end;
	};

end:
	k_free(data);

	return r;
}

/////////////// PUBLIC METHODS /////////////

CoapClient::CoapClient(int sock) {
	this->sock = sock;
}

CoapClient::CoapClient() {
#if defined(CONFIG_NET_IPV4)
	struct sockaddr_in addr;
	memset(&addr, 0, sizeof(addr));
	addr.sin_family = AF_INET;
	addr.sin_port = htons(8201);

	sock = socket(addr.sin_family, SOCK_DGRAM, IPPROTO_UDP);
#else
	struct sockaddr_in6 addr;
	memset(&addr, 0, sizeof(addr));
	addr.sin6_family = AF_INET6;
	addr.sin6_port = htons(8201); // TODO: try different port when this port is unavailable

	sock = socket(addr.sin6_family, SOCK_DGRAM, IPPROTO_UDP);
#endif

	if (sock < 0) {
		LOG_ERR("Failed to create UDP socket %d", errno);
		// TODO: throw exception
		return;
	}

	int r = bind(sock, (struct sockaddr *)&addr, sizeof(addr));
	if (r < 0) {
		LOG_ERR("Failed to bind UDP socket %d", errno);
		return;
	}
}

void CoapClient::close_coap_client() {
	(void)close(sock);
}

message_t CoapClient::send_message_and_wait_response(request_t request, int timeout) {
	LOG_DBG("destination address: %s \n", request.dst_host);

	send_simple_coap_request(request);

    message_t response = process_simple_coap_reply(timeout);
	
	return response;
}