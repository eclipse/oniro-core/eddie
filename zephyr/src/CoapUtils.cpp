/*
 * Copyright (c) 2022 Huawei Inc.
 *
 * SPDX-License-Identifier: Apache-2.0
 */

#include "CoapUtils.h"

#include <logging/log.h>
LOG_MODULE_REGISTER(coaputils, LOG_LEVEL_DBG);

uint8_t method_to_coap_method(method_t method) {
	switch (method) {
	case GET:
		return COAP_METHOD_GET;
	case POST:
		return COAP_METHOD_POST;
	case PUT:
		return COAP_METHOD_PUT;
	case DELETE:
		return COAP_METHOD_DELETE;
	}
	return -1;
}

method_t coap_method_to_method(uint8_t method) {
	switch (method) {
	case COAP_METHOD_GET:
		return GET;
	case COAP_METHOD_POST:
		return POST;
	case COAP_METHOD_PUT:
		return PUT;
	case COAP_METHOD_DELETE:
		return DELETE;
	}
	LOG_ERR("Invalid coap method");
	return GET;
}
